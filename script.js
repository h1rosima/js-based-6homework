class Product {
    constructor(name, price, discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
    }

    totalPrice() {
        let discountPrice = (this.price * this.discount) / 100;
        return (this.price - discountPrice);
    }

}

const laptop = new Product('Ноутбук', 1000, 10);
console.log(laptop.totalPrice());

//2
function greeting(user) {

    return (`Привет ${user.name}, мне ${user.age} лет. `)
}

let name = prompt('Введите ваше имя');
let age = prompt('Введите ваш возраст');

alert(greeting({name, age}));


//3

function cloneObject(object) {
    let clonedObject = {};

    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            if (typeof object[key] === 'object' && object[key] !== null) {
                clonedObject[key] = cloneObject(object[key]);
            } else {
                clonedObject[key] = object[key];
            }
        }
    }

    return clonedObject;
}
